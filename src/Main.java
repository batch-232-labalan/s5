import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");
        Phonebook phonebook = new Phonebook();
        phonebook.setName("John Doe");
        phonebook.setContactNumber("+63995522778");
        phonebook.setAddress("Quezon City");
        phonebook.setContacts(phonebook.getName()+" "+phonebook.getContactNumber()+" "+phonebook.getAddress());

        phonebook.setName("Jane Doe");
        phonebook.setContactNumber("+63994411667");
        phonebook.setAddress("Caloocan City");
        phonebook.setContacts(phonebook.getName()+" "+phonebook.getContactNumber()+" "+phonebook.getAddress());

        if(phonebook.getContacts().isEmpty()){
            System.out.println("The Phonebook is currently empty");
        }else{
            for(int i=0; i < phonebook.getContacts().size(); i++){
                System.out.println("=========================================");
                phonebook.browseContact();
            }
        }



    }
}