package com.zuitt.example;

import java.util.ArrayList;


public class Phonebook extends Contact{
    public ArrayList<String> contacts = new ArrayList<>();

    public Phonebook() {
    }

    public Phonebook(String name, String contactNumber, String address, ArrayList<String> contacts) {
        super(name, contactNumber, address);
        this.contacts = contacts;
    }

    public ArrayList<String> getContacts() {
        return this.contacts;
    }

    public void setContacts(String contacts) {
        this.contacts.add(contacts);

    }

    public void browseContact(){
        super.browse();
    }


}
