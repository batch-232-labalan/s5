package com.zuitt.example;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    //constructor
    public Contact(){};

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    //getter
    public String getName(){
        return this.name;
    }

    public String getContactNumber(){
        return this.contactNumber;
    }

    public String getAddress(){
        return this.address;
    }

    //setter
    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public void browse(){
        System.out.println(this.name+" has the following registered number");
        System.out.println(this.contactNumber);

        System.out.println(this.name+" has the following registered address");
        System.out.println(this.address);
    }


}
